from django.urls import path
from accounts.views import todo_list_list

urlpatterns = [
    path("", todo_list_list, name="todo_list_list"),
    ]